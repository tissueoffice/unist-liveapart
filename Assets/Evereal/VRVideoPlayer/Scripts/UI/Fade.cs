﻿/* Copyright (c) 2020-present Evereal. All rights reserved. */

using System.Collections;
using UnityEngine;

namespace Evereal.VRVideoPlayer
{
  [RequireComponent(typeof(Renderer))]
  public class Fade : MonoBehaviour
  {
    public float fadeDuration = 1.0f;
    private float delayDuration;
    private float delayBuffer=1.5f;
    public CommentManager commentManager;

    public delegate void FadeOutCompletedEvent();
    public event FadeOutCompletedEvent fadeOutCompleted = delegate { };

    public delegate void FadeInCompletedEvent();
    public event FadeInCompletedEvent fadeInCompleted = delegate { };

    public IEnumerator StartFadeOut()
    {
      Renderer fadeRenderer = GetComponent<Renderer>();
      Material fadeMaterial = fadeRenderer.material;
      Color startColor = new Color(fadeMaterial.color.r, fadeMaterial.color.g, fadeMaterial.color.b, 0f);
      Color targetColor = new Color(fadeMaterial.color.r, fadeMaterial.color.g, fadeMaterial.color.b, 1f);
      float fadeStartTime = Time.time;
      float fadeProgress;
      bool fading = true;

      while (fading)
      {
        yield return new WaitForEndOfFrame();
        fadeProgress = Time.time - fadeStartTime;
        if (fadeRenderer != null)
        {
          fadeRenderer.material.color = Color.Lerp(startColor, targetColor, fadeProgress / fadeDuration);
        }
        else
        {
          fading = false;
        }

        if (fadeProgress >= fadeDuration)
        {

          fading = false;
        }
      }
      delayDuration=commentManager.delayDuration+delayBuffer;
      Debug.Log("delayDuration: "+delayDuration);
      commentManager._DelayForComment();
      StartCoroutine("StartDelay");
    }

    public IEnumerator StartDelay(){
      yield return new WaitForSeconds(delayDuration);
      fadeOutCompleted();
    }

    public void StopDelay(){
      Debug.Log("StopDelay");
      StopCoroutine("StartDelay");
      commentManager.StopDelay();
      fadeOutCompleted();
    }

    public IEnumerator StartFadeIn()
    {
      Renderer fadeRenderer = GetComponent<Renderer>();
      Material fadeMaterial = fadeRenderer.material;
      Color startColor = new Color(fadeMaterial.color.r, fadeMaterial.color.g, fadeMaterial.color.b, 1f);
      Color targetColor = new Color(fadeMaterial.color.r, fadeMaterial.color.g, fadeMaterial.color.b, 0f);
      float fadeStartTime = Time.time;
      float fadeProgress;
      bool fading = true;

      while (fading)
      {
        yield return new WaitForEndOfFrame();
        fadeProgress = Time.time - fadeStartTime;
        if (fadeRenderer != null)
        {
          fadeRenderer.material.color = Color.Lerp(startColor, targetColor, fadeProgress / fadeDuration);
        }
        else
        {
          fading = false;
        }

        if (fadeProgress >= fadeDuration)
        {
          fading = false;
        }
      }
      fadeInCompleted();
    }
  }
}