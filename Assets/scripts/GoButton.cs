/* Copyright (c) 2020-present Evereal. All rights reserved. */
using UnityEngine.SceneManagement;

namespace Evereal.VRVideoPlayer
{
  public class GoButton : ButtonBase
  {
    public int selectedChapter=0;

    protected override void OnClick()
    {
        DataManager.Instance.currentChapter=selectedChapter;
        DataManager.Instance.isVideoScene=true;
        DataManager.Instance.GoToCertainScene("VideoScene");

    }
  }
}