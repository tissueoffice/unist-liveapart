/* Copyright (c) 2020-present Evereal. All rights reserved. */
using UnityEngine;
using System.Collections;
namespace Evereal.VRVideoPlayer
{
  public class Pause_Cancel_Button : ButtonBase
  {

      public PlayButton PlayButton;

    protected override void OnClick()
    {
        videoPlayerCtrl.ToggleVideoPlay();
        PlayButton.Toggle();
        
    }
   

  }
}