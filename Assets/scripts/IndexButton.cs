/* Copyright (c) 2020-present Evereal. All rights reserved. */


using UnityEngine;
namespace Evereal.VRVideoPlayer
{
  public class IndexButton : ButtonBase
  {
    
      public int selectedIndex=0;
    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Update()
    {
        if(selectedIndex==DataManager.Instance.currentVideoIndex){
            SetEnable();
        }else{
            SetDisable();
        }
    }

    public void SetEnable()
    {
      SpriteRenderer renderer = GetComponent<SpriteRenderer>();
      renderer.color = new Color(renderer.color.r, renderer.color.g, renderer.color.b, 1f);
    }

    public void SetDisable()
    {
      SpriteRenderer renderer = GetComponent<SpriteRenderer>();
      renderer.color = new Color(renderer.color.r, renderer.color.g, renderer.color.b, 0.1f);
    }

    protected override void OnClick()
    {
        DataManager.Instance.currentVideoIndex=selectedIndex;
        DataManager.Instance.SetCertainAudio(selectedIndex);
        videoPlayerCtrl.RestartVideo();
        Debug.Log("selected Index: "+selectedIndex);

        
    }
  }
}