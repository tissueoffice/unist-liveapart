    h          2019.4.17f1 ţ˙˙˙      ˙˙f!ë59Ý4QÁóB   í          7  ˙˙˙˙                 Ś ˛                       E                    Ţ  #                     . ,                     5   a                    Ţ  #                     . ,                      r                    Ţ  #      	               . ,      
               H Ť ˙˙˙˙             1  1  ˙˙˙˙   @           Ţ                     Q  j                    ń  J   ˙˙˙˙    Ŕ           1  1  ˙˙˙˙                Ţ                        j  ˙˙˙˙                \     ˙˙˙˙                H r   ˙˙˙˙               1  1  ˙˙˙˙   @            Ţ                      Q  j                     H w   ˙˙˙˙               1  1  ˙˙˙˙   @            Ţ                      Q  j                     H    ˙˙˙˙               1  1  ˙˙˙˙   @            Ţ                      Q  j                     y 
                      Ţ  #      !               . ,      "                   ˙˙˙˙#   @          1  1  ˙˙˙˙$               Ţ      %               . j     &               Ő    ˙˙˙˙'               1  1  ˙˙˙˙(    Ŕ            Ţ      )                  j  ˙˙˙˙*                H   ˙˙˙˙+               1  1  ˙˙˙˙,   @            Ţ      -                Q  j     .                y 
    /                 Ţ  #      0               . ,      1                 §      2    @            ž ś      3    @            Ţ  #      4               . ,      5               H ť   ˙˙˙˙6              1  1  ˙˙˙˙7   @            Ţ      8                Q  j     9                H Ć   ˙˙˙˙:              1  1  ˙˙˙˙;   @            Ţ      <                Q  j     =                H Ř   ˙˙˙˙>              1  1  ˙˙˙˙?   @            Ţ      @                Q  j     A              MonoImporter PPtr<EditorExtension> m_FileID m_PathID PPtr<PrefabInstance> m_ExternalObjects SourceAssetIdentifier type assembly name m_UsedFileIDs m_DefaultReferences executionOrder icon m_UserData m_AssetBundleName m_AssetBundleVariant     s    ˙˙ŁGń×ÜZ56 :!@iÁJ*          7  ˙˙˙˙                 Ś ˛                        E                    Ţ                       .                      (   a                    Ţ                       .                       r                    Ţ        	               .       
               H Ť ˙˙˙˙             1  1  ˙˙˙˙   @           Ţ                     Q  j                    H ę ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     ń  =   ˙˙˙˙              1  1  ˙˙˙˙               Ţ                       j  ˙˙˙˙               H   ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     y 
                    Ţ                       .                      y Q                       Ţ                       .                       Ţ  X      !                H i   ˙˙˙˙"              1  1  ˙˙˙˙#   @            Ţ      $                Q  j     %                H u   ˙˙˙˙&              1  1  ˙˙˙˙'   @            Ţ      (                Q  j     )              PPtr<EditorExtension> m_FileID m_PathID PPtr<PrefabInstance> m_DefaultReferences m_Icon m_ExecutionOrder m_ClassName m_Namespace                      \       ŕyŻ     `                                                                                                                                                                               ŕyŻ                                                                                    VREyeRaycaster    /* Copyright (c) 2017-present Evereal. All rights reserved. */

using System;
using UnityEngine;

namespace Evereal.VRVideoPlayer
{
  // In order to interact with objects in the scene
  // this class casts a ray into the scene and if it finds
  // a VRInteractiveItem it exposes it for other classes to use.
  // This script should be generally be placed on the camera.
  public class VREyeRaycaster : MonoBehaviour
  {
    // This event is called every frame that the user's gaze is over a collider.
    public event Action<RaycastHit> OnRaycasthit;

    private Transform cameraTransform;
    // Layers to exclude from the raycast.
    [SerializeField] private LayerMask exclusionLayers;
    // How far into the scene the ray is cast.
    [SerializeField] private float rayLength = 500f;
    // Optionally show the debug ray.
    [SerializeField] private bool showDebugRay;
    // Debug ray length.
    [SerializeField] private float debugRayLength = 5f;
    // How long the Debug ray will remain visible.
    [SerializeField] private float debugRayDuration = 1f;

    // The current interactive item
    private VRInteractiveItem currentInteractible;
    // The last interactive item
    private VRInteractiveItem lastInteractible;

    // Utility for other classes to get the current interactive item
    public VRInteractiveItem CurrentInteractible
    {
      get { return currentInteractible; }
    }

    private void Awake()
    {
      cameraTransform = GetComponent<Camera>().transform;
    }

    private void Update()
    {
      EyeRaycast();
    }

    private void EyeRaycast()
    {
      // Show the debug ray if required
      if (showDebugRay)
      {
        Debug.DrawRay(cameraTransform.position, cameraTransform.forward * debugRayLength, Color.blue, debugRayDuration);
      }

      // Create a ray that points forwards from the camera.
      Ray ray = new Ray(cameraTransform.position, cameraTransform.forward);
      RaycastHit hit;

      // Do the raycast forweards to see if we hit an interactive item
      if (Physics.Raycast(ray, out hit, rayLength, ~exclusionLayers))
      {
        // attempt to get the VRInteractiveItem on the hit object
        VRInteractiveItem interactible = hit.collider.GetComponent<VRInteractiveItem>();
        currentInteractible = interactible;

        // If we hit an interactive item and it's not the same as the last interactive item, then call Over
        if (interactible && interactible != lastInteractible)
          interactible.Over(hit.point);

        // Deactive the last interactive item
        if (interactible != lastInteractible)
          DeactiveLastInteractible();

        lastInteractible = interactible;

        if (OnRaycasthit != null)
          OnRaycasthit(hit);
      }
      else
      {
        // Nothing was hit, deactive the last interactive item.
        DeactiveLastInteractible();
        currentInteractible = null;
      }
    }

    private void DeactiveLastInteractible()
    {
      if (lastInteractible == null)
        return;

      lastInteractible.Out();
      lastInteractible = null;
    }

    private void HandleUp()
    {
      if (currentInteractible != null)
        currentInteractible.Up();
    }

    private void HandleDown()
    {
      if (currentInteractible != null)
        currentInteractible.Down();
    }

    private void HandleClick()
    {
      if (currentInteractible != null)
        currentInteractible.Click();
    }

    private void HandleDoubleClick()
    {
      if (currentInteractible != null)
        currentInteractible.DoubleClick();
    }
  }
}                       VREyeRaycaster     Evereal.VRVideoPlayer   